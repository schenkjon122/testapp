var appRoutes = angular.module('appRoutes', []);

appRoutes.config(['$routeProvider', '$locationProvider', function($routeProvide$
  $routeProvider
    .when('/', {
        templateUrl: './views/pages/home.client.view.html'
    })
    .when('/auth/signup', {
        templateUrl: './view/account/create-user.client.view.html',
        controller: 'AuthController',
        resolve: {
          skipIfLoggedIn: skipIfLoggedIn
        }
    })
    .when('/auth/login', {
        templateUrl: './view/account/login.client.view.html',
        controller: 'AuthController',
        resolve: {
          skipIfLoggedIn: skipIfLoggedIn
        }
    })
	.when('/logout', {
        templateUrl: null,
        controller: 'LogoutCtrl',
        resolve: {
          loginRequried: loginRequired
        }
    })
	.when('/account', {
        templateUrl: './view/account/edit-account.client.view.html',
        controller: 'ProfileController',
        resolve: {
          loginRequried: loginRequired
        }
    })
	.when('/page/about', {
        templateUrl: './view/page/about.client.view.html',
    })

	.otherwise({ redirectTo: '/'});

	function skipIfLoggedIn($q, $auth) {
		var deferred = $q.defer();
		if ($auth.isAuthenticated()) {
			deferred.reject();
		} else {
			deferred.resolve();
		}
		return deferred.promise;
	}

	function loginRequired($q, $location, $auth) {
		var deferred = $q.defer();
		if ($auth.isAuthenticated()) {
			deferred.resolve();
		} else {
			$location.path('/auth/login');
		}
        return deferred.promise;
	}
		$locationProvider.html5mode(true);
}]);
