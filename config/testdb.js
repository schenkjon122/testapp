var	mongoose = require('mongoose'),
	secrets  = require('./secrets');

var db = mongoose.connection;
mongoose.connect(secrets.db);

module.exports = {
	dbconnect: function(){
		db.on('error', console.error.bind( console, 'MONGODB CONNECTION ERROR'));
		db.once('open', function callback(){
			console.log('MongoDb Has Been Opened');
		});
	}
};
