module.exports = {
	db: process.env.MONGODB || process.env.MONOHQ_URL,
	TOKEN_SECRET: process.env.TOKEN_SECRET,
};

